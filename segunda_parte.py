'''Equipo 10
Integrantes:

Ignacio Sabino
Eduardo Palmiero
Tomas Guzman
Jonatan Rabinovitch
Mateo Cunha

'''

import random


def simular_equipo_a():
    '''Esta funcion se encarga de calcular cuanto tiempo tardo el equipo A en realizar una carrera'''
    tiempo_total = 0
    media = 6
    desvio_estandar = 1
    min = 6
    max = 7
    for i in range(3):
        '''Los tres primeros corredores se calculan de la misma forma'''
        velocidad_un_corredor = random.normalvariate(media, desvio_estandar)
        tiempo_en_100_metros = velocidad_un_corredor * 100
        tiempo_total += tiempo_en_100_metros
    velocidad_cuarto_corredor = random.uniform(min, max)
    tiempo_en_100_metros = (velocidad_cuarto_corredor * 100)
    tiempo_total += tiempo_en_100_metros
    return tiempo_total


def simular_equipo_b():
    '''Esta funcion se encarga de calcular cuanto tiempo tardo el equipo B en realizar una carrera'''
    tiempo_total = 0
    media = 6.3
    desvio_estandar = .6
    for i in range(4):
        '''Los cuatro corredores se calculan de la misma forma'''
        velocidad_un_corredor = random.normalvariate(media, desvio_estandar)
        tiempo_en_100_metros = velocidad_un_corredor * 100
        tiempo_total += tiempo_en_100_metros
    return tiempo_total


def calcular_probabilidad_de_que_gane_a(cantidad_simulaciones):
    '''Este metodo suma la cantidad de victorias de los equipos en el total de las simulaciones
     y devuelve la probabilidad de que gane el equipo A'''
    carreras_ganadas_a = 0
    carreras_ganadas_b = 0
    for i in range(cantidad_simulaciones):
        '''Simulo una carrera y comparo '''
        tiempo_total_a = simular_equipo_a()
        tiempo_total_b = simular_equipo_b()
        if (tiempo_total_a < tiempo_total_b):
            carreras_ganadas_a += 1
        else:
            carreras_ganadas_b += 1
    return carreras_ganadas_a / (carreras_ganadas_b + carreras_ganadas_a)


resultado_probabilidad = calcular_probabilidad_de_que_gane_a(200000)
print("La probabilidad de que gane A es de: " + str(resultado_probabilidad) + " es decir, un " + str(
    resultado_probabilidad * 100) + "%")
