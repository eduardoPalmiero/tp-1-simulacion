# Pylab is basically just Numpy and Matplotlib under a unified namespace. Learn about either of those and you will understand Pylab.
# Docu de histograma https://matplotlib.org/api/_as_gen/matplotlib.pyplot.hist.html
\

# Tambien se puede hacer esto utilizando Numpy
# import numpy
# valores_simulados_numpy = numpy.random.normal(media, desvio_estandar, cantidad_simulaciones)
'''https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.normal.html'''
# count, bins, ignored = pyplot.hist(valores_simulados_numpy, n_bins, density=True, color='g')
'''
Preparar el histograma con los valores de la normal simulados por numpy
El 2do parametro son los bins
https://stackoverflow.com/questions/33458566/how-to-choose-bins-in-matplotlib-histogram
El 3er parametro, no se bien, pero influye en la linea roja que se dibuja encima.
Lo interesante es que este metodo devuelve tres puntos que indican como esta armado el histograma, y uno de estos parametros, 'bins', es lo que luego podemos utilizar para armar la linea
'''
# funcion_de_densidad_de_probabilidad = 1 / (desvio_estandar * numpy.sqrt(2 * numpy.pi)) * numpy.exp(
#     - (bins - media) ** 2 / (2 * desvio_estandar ** 2))
# pyplot.plot(bins, funcion_de_densidad_de_probabilidad, linewidth=2, color='r')
'''
To plot es Trazar. 
Esto no lo pide el ejercicio pero queda bien cool. En este caso lo que vamos a trazar es una linea roja que siga la forma de la curva Gaussiana
Tengo entendido que lo que se usa es calcular la funcion de densidad de probabilidad.
Los ultimos dos parametros son de configuraciones visuales
El primer parametro es lo que queremos que grafique
'''
# pyplot.show()