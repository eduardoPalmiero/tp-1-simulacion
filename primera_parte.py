'''Equipo 10
Integrantes:

Ignacio Sabino
Eduardo Palmiero
Tomas Guzman
Jonatan Rabinovitch
Mateo Cunha

'''

import matplotlib.pyplot as pyplot
import random

media = 200
desvio_estandar = 30
n_bins = 100
'''Valores comunes de la distribucion
Bins: The bins parameter tells you the number of bins that your data will be divided into. You can specify it as an integer or as a list of bin edges.
Is the number of intervals you want to divide all of your data into, such that it can be displayed as bars on a histogram.
Mientras mas alto sea el numero, mas muestras toma para hacer el grafico y mas "redondeado" va a verse
'''

valores_modulo_random = []
valores_formula = []
'''El primer array es para probar la funcion normalvariate del modulo random. Tendra color azul oscuro.
El segundo array es para probar la formula vista en clase en forma manual. Tendra color por rojo.
'''


def simular(cantidad_simulaciones):
    '''Este metodo sirve para realizar las distintas simulaciones'''
    for i in range(cantidad_simulaciones):
        '''Esta es la forma mas sencilla de resolverlo.
        Este for se encarga de cada valor simulado'''
        valores_modulo_random.append(random.normalvariate(media, desvio_estandar))
        sumatoria_numeros_aleatorios = 0
        for j in range(0, 12):
            '''Esta es una forma mas a mano de hacerlo, en la que se ve lo que hace la formula para descubrir el valor simulado\
            Este valor se encarga de iterar 12 veces para generar un solo valor simulado'''
            sumatoria_numeros_aleatorios = sumatoria_numeros_aleatorios + random.random()
        valores_formula.append(media + desvio_estandar * (sumatoria_numeros_aleatorios - 6))
    figures, axes = pyplot.subplots(1, 2, sharey=True, tight_layout=True)
    axes[0].hist(valores_modulo_random, n_bins, color='b', density=True)
    axes[1].hist(valores_formula, n_bins, color='r', density=True)
    pyplot.ylim(0, 0.03)
    pyplot.show()
    '''Estos ultimos metodos son configuracion de pyplot para graficar'''


simular(1000)
simular(10000)
simular(100000)
simular(200000)
